using Godot;
using System;

public class ChangeScene : Button
{
	[Export] private readonly string scenePath = "";
	public override void _Pressed()
	{
		GetTree().ChangeScene(scenePath);
	}
}
