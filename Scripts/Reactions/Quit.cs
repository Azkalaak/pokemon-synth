using Godot;
using Aurora;

public class Quit : Button
{
	public override void _Pressed()
	{
		Log.Debug("Quitting from main menu");
		GetTree().Quit();
	}
}
