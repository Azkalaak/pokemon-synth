using Godot;
using System;
using System.Collections.Generic;
using Aurora;

public class ChangeLanguage : Button {
	[Export] private readonly bool next = true;

	public override void _Pressed() {
		String locale = TranslationServer.GetLocale();
		Godot.Collections.Array loadedLocales = TranslationServer.GetLoadedLocales();

		int currentIndex = this.GetLocaleIndex(loadedLocales, locale);
		int newIndex = this.GetNewIndex(loadedLocales, currentIndex);

		String newLocale = (String)loadedLocales[newIndex];
		Log.Debug("Changing locale to : " + newLocale);
		TranslationServer.SetLocale(newLocale);
	}

	private int GetLocaleIndex(Godot.Collections.Array loadedLocales, String locale) {
		String normalizedLocale = this.GetNormalizedLocale(locale);
		return loadedLocales.IndexOf(normalizedLocale);
	}

	private String GetNormalizedLocale(String locale) {
		String normalizedLocale = locale;
		int underscoreIndex = locale.IndexOf("_");
		if (underscoreIndex != -1) {
			normalizedLocale = locale.Substring(0, underscoreIndex);
		}
		return normalizedLocale;
	}

	private int GetNewIndex(Godot.Collections.Array loadedLocales, int currentIndex) {
		int newIndex = this.next ? currentIndex + 1 : currentIndex - 1;
		if (newIndex == loadedLocales.Count) {
			return 0;
		}
		if (newIndex < 0) {
			return loadedLocales.Count - 1;
		}
		return newIndex;
	}
}
