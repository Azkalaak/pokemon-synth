using Godot;

public class ChangeSceneOnKeyPressed : Node
{
	[Export] private readonly string scenePath = "";
	[Export] private readonly string action;
	
	public override void _Input(InputEvent ev)
	{
		
		if (ev.IsActionPressed(action))
			GetTree().ChangeScene(scenePath);
	}
}
