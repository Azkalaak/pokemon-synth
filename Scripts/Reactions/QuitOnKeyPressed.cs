using Godot;
using Aurora;

public class QuitOnKeyPressed : Node
{
	[Export] private readonly string action;
	
	public override void _Input(InputEvent ev)
	{
		
		if (ev.IsActionPressed(action)){
            Log.Debug("Quitting from main menu with escape key");
		    GetTree().Quit();
        }
	}
}
