using Godot;
using System;

public enum TypeEnum { NONE, LIGHT, FIRE, WATER, ELECTRIC, GROUND, ROC, }

public class Type : Resource
{
	[Export] public string Name { get; set; }
	[Export] public string Description { get; set; }
	[Export] public TypeEnum[] Weaknesses { get; set; }
	[Export] public TypeEnum[] Resistance { get; set; }
	[Export] public TypeEnum[] Invulnerability { get; set; }
}
