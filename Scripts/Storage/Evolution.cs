using Godot;

public enum EvolutionCond { NO_EVOLUTION, LEVEL, HAPPINESS, FIRE_STONE, WATER_STONE, MOON_STONE, BROKEN_STONE, DAYTIME, ZONE, OBJECT, BEAUTY, SEX, ATTACK };
public enum Sex { MALE, FEMALE, UNKNOWN };

public class Evolution : Resource
{
	[Export] public Pokemon evolution = null;

	[Export] public EvolutionCond[] Condition { get; set; }

	[Export] public int level = 0;

	// borders possible
	[Export] public Vector2 Time { get; set; }
	[Export] public Sex Sex { get; set; } = Sex.UNKNOWN;
}
