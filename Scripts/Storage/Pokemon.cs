using Godot;
using System;
using System.Collections.Generic;

public class Pokemon : Resource
{
	[Export] private int Id { get; set; }
	[Export] private string Name { get; set; }
	[Export] public TypeEnum Type1 { get; set; }
	[Export] public TypeEnum Type2 { get; set; }
	[Export] private int Hp { get; set; }
	[Export] private int Attack { get; set; }
	[Export] private int Defense { get; set; }
	[Export] private int AttackSpe { get; set; }
	[Export] private int DefenseSpe { get; set; }
	[Export] private int Speed { get; set; }
	[Export] public Evolution Evolution { get; set; }
	
	// either link evolution there, or make another list containing every single evolution
}
