using Godot;
using System;
using System.Threading;
using Aurora;

public class TestsAz : Node
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first Aurora.Time.
	public override void _Ready()
	{
		Log.Info(Aurora.Time.ActualTime.ToString());
		System.Threading.Thread.Sleep(1000);
		Aurora.Time.RefreshDate();
		Log.Info(Aurora.Time.ActualTime.ToString());
		Log.Info(Aurora.Time.ActualSeason.ToString());
		Aurora.Time.NextSeason();
		Log.Info(Aurora.Time.ActualSeason.ToString());
		Aurora.Time.NextSeason();
		Log.Info(Aurora.Time.ActualSeason.ToString());
		Aurora.Time.NextSeason();
		Log.Info(Aurora.Time.ActualSeason.ToString());
		Aurora.Time.NextSeason();
		Log.Info(Aurora.Time.ActualSeason.ToString());

		Aurora.Time.NextSeason();
		Log.Info(Aurora.Time.ActualSeason.ToString());

		Aurora.Time.Reset();
		Log.Info(Aurora.Time.ActualSeason.ToString());

		Log.Info(Aurora.Time.ActualTime.DayOfWeek.ToString());

		Log.Warning("This is a warning test");
		Log.Error("This is an error test");
		// This message is translated thanks to the Tr() function
		Log.Debug(Tr("TEST_MESSAGE"));
	}

	//  // Called every frame. 'delta' is the elapsed Aurora.Time since the previous frame.
	//  public override void _Process(float delta)
	//  {
	//      
	//  }
}
