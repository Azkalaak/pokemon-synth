using Godot;
using System;

public enum MovementType {DIRECTIONS_4, DIRECTIONS_8, DIRECTIONS_TILES };
public class Movement : KinematicBody2D
{
	[Export] public MovementType Type { get; set; }
	[Export] public int walkSpeed = 60;
	[Export] public int runSpeed = 120;

	private int actualSpeed;

	private Vector2 velocity;

	private void GetInputEight()
	{
		velocity.x = 0;
		velocity.y = 0;

		actualSpeed = Input.IsActionPressed("game_toggle_speed") ? runSpeed : walkSpeed;

		if (Input.IsActionPressed("game_right"))
			velocity.x += 1;

		if (Input.IsActionPressed("game_left"))
			velocity.x -= 1;

		if (Input.IsActionPressed("game_down"))
			velocity.y += 1;

		if (Input.IsActionPressed("game_up"))
			velocity.y -= 1;

		// Speed is applied there, so there is no issue of speed being variable prior to this.
		velocity = velocity.Normalized() * actualSpeed;
	}

	/// <summary>
	/// Movement in all 8 directions (up down left right + diagonals)
	/// </summary>
	private void EightMovement()
	{
		GetInputEight();
		velocity = MoveAndSlide(velocity);
	}
	
	public override void _PhysicsProcess(float delta)
	{
		EightMovement();
	}
}
