using System;

namespace Aurora {
	public enum Season { SPRING, SUMMER, AUTUMN, WINTER };
	
	public static class Time
	{
		private static DateTime _actualTime;
		public static Season ActualSeason { get; set; }
		public static DateTime ActualTime { get { return _actualTime; }}
		
		static Time()
		{
			Reset();
		}
		
		public static void RefreshDate()
		{
			_actualTime = DateTime.Now;
		}
		
		/// <summary>
		/// Reset class (refresh date + goes to spring season)
		/// </summary>
		public static void Reset()
		{
			ActualSeason = Season.SPRING;
			RefreshDate();
		}
		
		/// <summary>
		/// Advance one season (returns to spring after winter)
		/// </summary>
		public static void NextSeason()
		{
			switch(ActualSeason)
            {
				case Season.SPRING: ActualSeason = Season.SUMMER; break;
				case Season.SUMMER: ActualSeason = Season.AUTUMN; break;
				case Season.AUTUMN: ActualSeason = Season.WINTER; break;
				case Season.WINTER: ActualSeason = Season.SPRING; break;
            }
		}
	}
}
