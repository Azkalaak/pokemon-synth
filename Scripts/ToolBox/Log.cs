using Godot;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace Aurora
{
	public static class Log
	{
		/// <summary>
		/// write the messages either to a log file or the console.
		/// </summary>
		/// <param name="message">the message to print</param>
		private static void Write(params string[] message)
		{
			string tmp = "";
			for (int i = 0; i < message.Length; i++)
			{
				tmp += message[i];
			}
			GD.Print(tmp);
		}

		public static void Info(string message)
		{
			Write("[ Information ] : ", message);
		}

#if DEBUG
		/// <summary>
		/// send a warning message. does not print in release
		/// </summary>
		/// <param name="message">the message to print</param>
		/// <param name="memberName"></param>
		/// <param name="lineNumber"></param>
		/// <param name="filePath"></param>
		public static void Warning(string message, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			Write("[   Warning   ] : ", message, "\n\t\t- From function ", memberName, " In file '", filePath, "' l:", lineNumber.ToString());
		}

		/// <summary>
		/// send an error message. does not print in release
		/// </summary>
		/// <param name="message">the message to print</param>
		/// <param name="memberName"></param>
		/// <param name="lineNumber"></param>
		/// <param name="filePath"></param>
		public static void Error(string message, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
		{
			Write("[    Error    ] : ", message, "\n\t\t- From function ", memberName, " In file '", filePath, "' l:", lineNumber.ToString());
		}

		/// <summary>
		/// send a debug message. does not print in release
		/// </summary>
		/// <param name="message">the message to print</param>
		public static void Debug(string message)
		{
			Write("[    Debug    ] : ", message);
		}
#else
		public static void Warning(string message, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "") {}
		public static void Error  (string message, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "") {}
		public static void Debug(string message) {}
#endif
	}
}
